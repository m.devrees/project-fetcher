import paramiko
from utils.logger import Logger


class SshClient:
    client = None
    logger = None

    def __init__(self):
        self.logger = Logger().init_logger('ssh')
        self.client = paramiko.SSHClient()
        # add to known hosts
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    def connect(self, cmd, hostname, username, port=22, password=None, key_filename=None):
        try:
            self.logger.info("Connecting ssh client {}".format(hostname))

            self.client.connect(hostname=hostname,
                                port=port,
                                username=username,
                                password=password,
                                key_filename=key_filename
                                )
            self._execute_command(cmd)
        except Exception as error:
            self.logger.critical("Something went wrong with ssh")
            self.logger.critical(error)
            print("[!] Cannot connect to the SSH Server")
            exit()

    def _execute_command(self, cmd):
        # print("=" * 50, cmd, "=" * 50)
        self.logger.info("Executing command {}".format(cmd))
        stdin, stdout, stderr = self.client.exec_command(cmd)
        print(stdout.read().decode())
        # configFileContent = stdout.read().decode()
        err = stderr.read().decode()
        if err:
            print(err)
