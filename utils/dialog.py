def dialog_input(d, text, init=""):
    v = None
    while v is None or v == "":
        _, v = d.inputbox(text, init=init)
        v = v.strip()
        if v is None or v == "":
            d.msgbox("Value can not be empty")
    return v


def dialog_checklist(d, text, choices, title="", backtitle="", allow_empty=False, empty_cb=None):
    v = []
    q = None
    while v is None or len(v) <= 0:
        q, v = d.checklist(
            text,
            choices=choices,
            title=title,
            backtitle=backtitle,
        )
        if q is not d.OK:
            d.msgbox("Hmm... something went wrong...")
            continue

        if not allow_empty and len(v) <= 0:
            if empty_cb:
                if empty_cb(d):
                    break
            else:
                d.msgbox("No value selected")
    return q, v
