import logging
import os

class Logger:
    log_format = None

    def __init__(self):
        self.log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

    def init_logger(self, name):
        return self._get_logger(name)

    def _get_logger(self, name):
        logger = logging.getLogger(name)
        if not os.path.exists('logs'):
            os.makedirs('logs')
        fh = logging.FileHandler('logs/{}.log'.format(name))
        fh.setLevel(logging.DEBUG)
        formatter = logging.Formatter(self.log_format)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        fh.setLevel(logging.DEBUG)
        logger.setLevel(logging.DEBUG)
        return logger
