# X-Com Project Fetcher
Automatically clone magento2 projects, composer install and setup db connection. Please read the requirements and setup below carefully, otherwise this tool may not work the way you expect it to.

# Setup
## Requirements
- python3.8 (or up)
- [pip](https://pypi.org/project/pip/)

### Before
Before attempting installation, to upload your public ssh key to 
[Bitbuckets ssh key manager](https://bitbucket.org/account/settings/ssh-keys/). If you
have not yet created an ssh-key-pair, run `ssh-keygen` in your terminal and upload the contents of
`cat ~/.ssh/id_rsa.pub` to bitbucket.  

Also, it is required to have your docker containers up and running. The tool attempts to connect to your docker containers as composer is required to set up the project.

[**WIP not functioning yet**] In order to fetch mysqldumps from remote servers, the ssh key must also be added to service panels of hosting providers ([Byte SSH management](https://service.byte.nl/sshkeymanager/) and [Hipex SSH management](https://pack.hipex.io/ssh-key-management))

## Installation
```bash
$ git clone git@bitbucket.org:m.devrees/project-fetcher.git
$ cd project-fetcher
$ pip install -r requirements.txt --user
$ cp config.ini.sample config.ini
```
You can now edit the config.ini file which is used to provide basic configuration to the tool.

## Config
There's a config.ini.sample file provided, please rename it config.ini and adjust if necessary.  
To add new projects to the setup, please edit project/project.py and expand the team_projects dict.

## Running the project
Execute the main.py file
```bash
$ python3 main.py
```
and follow the on-screen instructions.

# About
- This project is built with Python3.8
```bash
$ python3
Python 3.8.10 (default, Jun  2 2021, 10:49:15) 
[GCC 9.4.0] on linux
Type "help", "copyright", "credits" or "license" for more information
```