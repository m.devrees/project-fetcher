from dataclasses import dataclass
from project.version import PhpVersion, MysqlVersion
from project.hosts import Hipex, Hypernode, BroadHorizon, BaseProvider


@dataclass
class Project:
    name: str
    php_version: PhpVersion
    mysql_version: str
    staging_server: BaseProvider
    mysql_prefix: str = ""

    def __str__(self):
        return self.name


teams_projects = {
    'ecommerce': [
        Project('peijs', PhpVersion.PHP73, MysqlVersion.MYSQL57.value, Hypernode(hostname="peijs.hypernode.io")),
        Project('cosinta', PhpVersion.PHP74, MysqlVersion.MYSQL80.value, Hypernode(hostname="cosintadev.hypernode.io")),
        Project('nutramin', PhpVersion.PHP73, MysqlVersion.MYSQL57.value, Hipex("")),
        Project('verhaak', PhpVersion.PHP73, MysqlVersion.MYSQL57.value, Hypernode(hostname="verhaak.hypernode.io")),
        Project('holbox', PhpVersion.PHP73, MysqlVersion.MYSQL57.value, Hypernode(hostname="holbox.hypernode.io")),
        Project('roman-techno', PhpVersion.PHP74, MysqlVersion.MYSQL80.value, Hipex("", workdir=""), mysql_prefix="mg_"),
        Project('profilplast', PhpVersion.PHP73, MysqlVersion.MYSQL57.value, BroadHorizon("")),
        Project('toi-toys', PhpVersion.PHP73, MysqlVersion.MYSQL57.value, Hypernode(hostname="m2toitoys.hypernode.io")),
        Project('leurs', PhpVersion.PHP74, MysqlVersion.MYSQL80.value, Hipex("", workdir="")),
        Project('bpg', PhpVersion.PHP73, MysqlVersion.MYSQL57.value, Hypernode(hostname="bpg.hypernode.io")),
        Project('bpg-ecommerce', PhpVersion.PHP73, MysqlVersion.MYSQL57.value, Hypernode(hostname="bpg.hypernode.io")),
        Project('brentjens', PhpVersion.PHP74, MysqlVersion.MYSQL80.value, Hipex("", workdir="")),
        Project('plusman', PhpVersion.PHP73, MysqlVersion.MYSQL57.value, Hipex(hostname="", workdir="")),
        Project('mertens-groep', PhpVersion.PHP74, MysqlVersion.MYSQL80.value, Hipex(hostname="", workdir="")),
        Project('georgeatwork', PhpVersion.PHP73, MysqlVersion.MYSQL80.value, Hypernode(hostname="m2georgeatwork.hypernode.io")),
        Project('limtrade', PhpVersion.PHP74, MysqlVersion.MYSQL80.value, Hipex(hostname="production1197.hipex.io", workdir="/home/staging/domains/staging-nl.limtrade.nl/dep_stage/current", username="staging")),
        Project('goessenswijnen', PhpVersion.PHP73, MysqlVersion.MYSQL57.value, Hipex(hostname="", workdir="/home/staging/domains/staging.goessens.com/dep_stage/current")),
        Project('dancohr', PhpVersion.PHP74, MysqlVersion.MYSQL80.value, Hipex("", workdir=""))
    ],
    'itix': [
       
    ]
}

project_index = {p.name: p for ps in teams_projects.values() for p in ps}
project_php_versions = list(set([p.php_version.value for ps in teams_projects.values() for p in ps]))
