from dataclasses import dataclass


@dataclass
class BaseProvider:
    hostname: str
    port: str
    workdir: str

    def fetch_mysqldump(self):
        pass


@dataclass
class Hipex(BaseProvider):
    port: str = 339
    workdir: str = ""
    username: str = ""


@dataclass
class Hypernode(BaseProvider):
    port: str = 22
    workdir: str = "/data/web/dep_stage/current"
    username: str = "app"


@dataclass
class BroadHorizon(BaseProvider):
    port: str = 22
    workdir: str = ""
