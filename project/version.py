from enum import Enum


class PhpVersion(Enum):
    PHP71 = 'php71'
    PHP72 = 'php72'
    PHP73 = 'php73'
    PHP74 = 'php74'
    PHP80 = 'php80'


class MysqlVersion(Enum):
    MYSQL57 = "MYSQL5.7"
    MYSQL80 = "MYSQL8.0"
