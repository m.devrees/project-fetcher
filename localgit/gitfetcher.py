import errno
import os

from git import Repo, GitError

from utils.logger import Logger


class GitFetcher(object):
    SUCCESS = 0
    FAILED = 1
    SKIPPED = 2

    repo_dir = None

    logger = None
    log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

    _dc = None
    _repo = None

    def __init__(self, project_name, repo_dir):
        self.project_name = project_name
        self.repo_dir = repo_dir
        self.logger = Logger().init_logger('fetcher')

    @staticmethod
    def mkdir_p(path):
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno == errno.EEXIST:
                pass
            else:
                raise

    def run(self):
        project = self.project_name
        self.logger.info("Running `{}`".format(project))

        if not os.path.exists(self.repo_dir) or (os.path.isdir(self.repo_dir) and len(os.listdir(self.repo_dir)) == 0):
            self.mkdir_p(self.repo_dir)
            try:
                repo = Repo.clone_from("git@bitbucket.org:x-com/{}".format(project), self.repo_dir)
                repo.git.checkout('develop')
            except GitError as e:
                self.logger.critical(str(e))
                return self.FAILED
        # if dir not empty, used for updating
        elif len(os.listdir(self.repo_dir)) != 0:
            try:
                repo = Repo(self.repo_dir)
                if len(repo.head.commit.diff()) > 0:
                    self.logger.info("Skipping `{}`, changes detected".format(project))
                    return self.SKIPPED
                repo.remotes.origin.pull()
            except GitError as e:
                self.logger.critical(str(e))
                return self.FAILED
        return self.SUCCESS
