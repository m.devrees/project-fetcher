#! /usr/bin/env python3
import configparser
import subprocess
import locale
import os
import sys

from localgit.gitfetcher import GitFetcher
from project.project import teams_projects, project_index, project_php_versions, PhpVersion
from localdocker.localdocker import LocalDocker
from ssh.ssh_client import SshClient
from utils.dialog import dialog_checklist, dialog_input
from utils.logger import Logger

try:
    from dialog import Dialog
except ImportError:
    print("You're missing one of the packages, please run pip install -r requirements.txt or check the README.md file",
          file=sys.stderr)
    sys.exit(1)

# This is almost always a good thing to do at the beginning of your programs.
locale.setlocale(locale.LC_ALL, '')


class Main:
    _docker = None
    _ssh = None
    config = None
    logger = None

    def __init__(self):
        self.config = configparser.ConfigParser()
        # write config.ini if not initially found
        if not os.path.exists('config.ini'):
            self.config.read('config.ini.sample')
            self.config.write(open('config.ini', 'w'))
        self._docker = LocalDocker()
        self.logger = Logger().init_logger('main')
        self._ssh = SshClient()

    def run(self):
        # You may want to use 'autowidgetsize=True' here (requires pythondialog >= 3.1)
        d = Dialog(dialog="dialog", autowidgetsize=True)

        # Dialog.set_background_title() requires pythondialog 2.13 or later
        d.set_background_title("project fetcher")

        self.config.read('config.ini')
        result = self.config['USER'].get('installdir', os.path.join(os.getenv("HOME"), "x-com"))
        user_dir = dialog_input(
            d,
            "Select root dir",
            init=result
        )
        root_dir = os.path.join(user_dir, "data/shared/sites")

        if not os.path.exists(root_dir):
            d.msgbox(
                "Cannot run whilst not having done init setup. See https://bitbucket.org/x-com/docker-setup and run "
                "install-dialog.sh")
            sys.exit(1)

        project_names = project_index.keys()

        _, teams = dialog_checklist(
            d,
            "Select team(s)",
            choices=[(t, "", False) for t in teams_projects.keys()],
            title="Teams",
            empty_cb=lambda d: d.yesno("Empty selection, continue anyway?") == d.OK
        )
        selected_projects = [p.name for t in teams for p in teams_projects[t]]
        _, projects = dialog_checklist(
            d,
            text="Select projects",
            choices=[
                (project, "", project in selected_projects) for project in project_names
            ],
            title="Teams",
            empty_cb=lambda d: d.yesno("Empty selection, continue anyway?") == d.OK
        )
        if d.yesno("Missing any projects?") == d.OK:
            v = None
            while v is None or v == "":
                q, v = d.inputbox("Additional project name(s)", help_button=True)
                if q == d.HELP:
                    d.msgbox("Add the project names, space separated")
                elif q == d.OK:
                    v = v.strip()
                    if v is None or v == "":
                        if d.yesno("Empty value, cancel adding extra projects?") == d.OK:
                            break
                elif q == d.CANCEL:
                    break
                else:
                    d.msgbox("Something went wrong...")
                    sys.exit(1)
            else:
                projects += v.split(" ")

        has_databases = False
        if d.yesno("Have all databases?") == d.OK:
            has_databases = True

        if len(projects) <= 0:
            d.msgbox("So no projects...? exiting")
            sys.exit(1)

        projects = [project_index[p] for p in projects]
        results = {}
        d.mixedgauge(
            text="Check the log files if something fails",
            title="Building",
            percent=0,
            elements=[(p.name, results[p.name] if p.name in results.keys() else "Pending") for p in projects]
        )

        d.gauge_start()
        # docker_config = self.config['DOCKER']
        # downgrade composer
        self.fix_composer()
        self.install_hirak_prestissimo()

        pkey = self.config['USER'].get('ssh_private_key', os.path.join(os.getenv("HOME"), ".ssh/id_rsa"))

        for i, project in enumerate(projects):
            d.mixedgauge(
                text="Check the log files if something fails",
                title="Building",
                percent=round(100 / (len(projects) / i)) if i > 0 else 0,
                elements=[(p.name, results[p.name] if p.name in results.keys() else "Pending") for p in projects]
            )
            # ain't pretty but it works
            repo_dir = os.path.join(root_dir, project.name)
            fetcher = GitFetcher(project.name, repo_dir=repo_dir)
            results[project.name] = fetcher.run()

            mysql_config = self.config[project.mysql_version]

            execute_order = {
                'composer': "{} install -q".format(self.config['DOCKER'].get('composer', '/home/web/bin/composer')),
                'create-schema': "mysql -u{} -p{} -h{} -P{} -e 'CREATE DATABASE IF NOT EXISTS `magento2_{}_develop`'".format(
                    mysql_config.get('user', 'root'),
                    mysql_config.get('password', 'xcom'),
                    mysql_config.get('host', '127.0.0.1'),
                    mysql_config.get('port', '3306'),
                    project.name
                )
                # TODO fetch a db dump from staging environment
                # 'magento-install': self.magento_install_command(project)
            }
            self._ssh.connect("uptime",
                              hostname=project.staging_server.hostname,
                              port=project.staging_server.port,
                              username=project.staging_server.username,
                              key_filename=pkey
                              )
            sys.exit(0)
            if has_databases:
                execute_order.update({'magento-install': self.magento_install_command(project)})
            for _, cmd in execute_order.items():
                self.logger.info(cmd)
                self._docker.execute(project.php_version.value, cmd, project.name)

        self._docker.restart_container('x-com-nginx-1')

        d.mixedgauge(
            text="Check the log files if something fails",
            title="Building",
            percent=100,
            elements=[(p.name, results[p.name] if p.name in results.keys() else "Pending") for p in projects]
        )

        d.gauge_stop()

    def fix_composer(self):
        for _version in project_php_versions:
            # downgrade to composer 1 if project not php74
            if _version not in [PhpVersion.PHP74.value, PhpVersion.PHP80.value]:
                self._docker.execute(_version, "{} self-update --1".format(
                    self.config['DOCKER'].get('composer', '/home/web/bin/composer')
                ))
            else:
                self._docker.execute(_version, "{} self-update --2".format(
                    self.config['DOCKER'].get('composer', '/home/web/bin/composer')
                ))

    def install_hirak_prestissimo(self):
        for _version in [PhpVersion.PHP71.value, PhpVersion.PHP72.value, PhpVersion.PHP73.value]:
            # downgrade to composer 1 if project not php74
            self._docker.execute(_version, "{} global require hirak/prestissimo".format(
                self.config['DOCKER'].get('composer', '/home/web/bin/composer')
            ))

    def magento_install_command(self, project):
        pass
        # TODO this should be finished
        magento_config = self.config['MAGENTO2']
        mysql_config = self.config[project.mysql_version]
        xcomuser = subprocess.run(["cat", "/etc/xcomuser"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        # cleanup-database should run empty installer
        installer = "bin/magento setup:install --backend-frontname='admin' --db-host='{}:{}' " \
                    "--db-name='magento2_{}_develop' --db-user='{}' --db-password='{}' " \
                    "--base-url='https://{}.{}.o.xotap.nl/' " \
                    "--timezone='UTC' --admin-firstname='{}' --admin-lastname='{}' --admin-email='{}' " \
                    "--admin-user='{}' --admin-password='{}' --currency='EUR' --language='en_US' " \
                    "--use-rewrites=1 " \
                    "--db-prefix='{}'".format(mysql_config.get('host', '127.0.0.1'),
                                              mysql_config.get('port', '3306'),
                                              project.name,
                                              mysql_config.get('user', 'root'),
                                              mysql_config.get('password', 'xcom'),
                                              project.name,
                                              xcomuser.stdout.strip(),
                                              magento_config.get('admin_firstname', 'xcom'),
                                              magento_config.get('admin_lastname', 'xcom'),
                                              magento_config.get('admin_email', 'xcom'),
                                              magento_config.get('admin_user', 'xcom'),
                                              magento_config.get('admin_password', 'xcom'),
                                              project.mysql_prefix
                                              )
        return installer


if __name__ == "__main__":
    Main().run()
