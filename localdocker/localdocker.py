import sys
from configparser import ConfigParser
from utils.logger import Logger

try:
    import docker
    from docker.errors import NotFound, BuildError, APIError
except ImportError as e:
    print(e)
    print("You're missing one of the packages, please run pip install", file=sys.stderr)
    sys.exit(1)


class LocalDocker:
    dc: docker
    logger = None
    config_object = None

    def __init__(self):
        self.dc = docker.from_env()
        self.logger = Logger().init_logger('localdocker')
        self.config_object = ConfigParser()
        self.config_object.read("config.ini")

    def execute(self, container, command, project=None):
        container_name = "docker_{}_1".format(container.replace('.', ''))
        try:
            _container = self.dc.containers.get(container_name)
            docker_config = self.config_object['DOCKER']
            workdir = None
            if project is not None:
                workdir = "{}/{}".format(docker_config.get("workdir", '/data/shared/sites'), project)
            _, result = _container.exec_run(command, workdir=workdir)
            self.logger.info(result)
        except (NotFound, APIError) as e:
            self.logger.critical(str(e))

    def restart_container(self, container_name):
        try:
            _container = self.dc.containers.get(container_name)
            _container.restart()
            self.logger.info("restarted nginx")
        except (NotFound, APIError) as e:
            self.logger.critical(str(e))
